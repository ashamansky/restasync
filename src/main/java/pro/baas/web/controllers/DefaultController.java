package pro.baas.web.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pro.baas.models.Car;
import pro.baas.services.CarLookupService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@RestController
@RequestMapping(value = "/")
@Slf4j
public class DefaultController {

	@Autowired
	private CarLookupService carLookupService;

	@RequestMapping(value = "/car", method = RequestMethod.GET)
	public Car index() throws InterruptedException, ExecutionException, TimeoutException {
		CompletableFuture<Car> carFuture = carLookupService.getACar("Audi");
		CompletableFuture.allOf(carFuture);
		log.info("Finished building car");
		return carFuture.get(50, TimeUnit.SECONDS);
	}
}
