package pro.baas.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pro.baas.models.Car;
import pro.baas.services.CarLookupService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class CarLookupServiceImpl implements CarLookupService {

	@Autowired
	private Executor executor;

	@Async
	@Override
	public CompletableFuture<Car> getACar(String name) throws InterruptedException {
		return	CompletableFuture.supplyAsync(() -> {
			String finalName = name +  Thread.currentThread().getName();
			log.error("Start creating a car {}", finalName);
			try {
				TimeUnit.SECONDS.sleep(40);
			} catch (InterruptedException e) {
				log.error(e.getMessage());
			} finally {
				return new Car(finalName);
			}
		}, executor);
	}
}
