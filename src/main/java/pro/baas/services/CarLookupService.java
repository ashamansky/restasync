package pro.baas.services;

import org.springframework.scheduling.annotation.Async;
import pro.baas.models.Car;

import java.util.concurrent.CompletableFuture;

public interface CarLookupService {
	@Async
	CompletableFuture<Car> getACar(String name) throws InterruptedException;
}
